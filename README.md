# PYTILA

## Logo

The logo is derived from [openclipart](https://openclipart.org/): [Black Bird](https://openclipart.org/detail/48841/black-bird), from [redccshirt](https://openclipart.org/user-detail/redccshirt), public domain.


## License

Pytila is licensed under the terms of the Apache Licence 2.0. See the [LICENSE file](https://framagit.org/lamessen/PyTiLa/blob/d4bc894f8cce5107bad806b94716ea0117bf483a/LICENSE).