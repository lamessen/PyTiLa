#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 20:03:05 2016
@author: lam
"""

import os
from Tkinter import Tk
import tkFileDialog 
import Image,ImageDraw, ImageFont


def askdirectory():
    """ Récupère le répertoire de travail """
    
    root = Tk()
    root.withdraw()
    rep = tkFileDialog.askdirectory(parent=root, initialdir=os.path.abspath(os.curdir), title="Sélectionner le repertoire")
    
    # Si pas un dossier, fin de traitement
    if not os.path.isdir(rep):
        return  
    return rep

def watermark(im,message):
    """Watermark image im with message."""

    # Taille des images    
    size_font = im.size[1]/20
    size_mess = int(len(message)*size_font*0.58)


        
    # Ajout du texte sur l'image       
    draw = ImageDraw.Draw(im)
    font = ImageFont.truetype('verdanai.ttf',im.size[1]/20)
    
    
    # Calcul des coordonnées
    size_mess = draw.textsize(message, font=font)   
    
    p1 = ((int(im.size[0]-size_mess[0])/2),int(im.size[1]*0.90))
    p2 = (p1[0]+size_mess[0] , p1[1]+ size_mess[1]+5)
    
    
    draw.rectangle((p1,p2),fill="#808080")
    draw.text( p1, message, font=font, fill='black')  
    del draw

def resize(im,pourcent):
    '''resize image im with the pourcent'''

    # Calcul de la nouvelle taille
    dimension = im.size
    dimension = (int(dimension[0]*pourcent),int(dimension[1]*pourcent))
    
    # Traitement
    im = im.resize(dimension, Image.ANTIALIAS)
    return im

def rotate(im,rotation):
    ''' rotation of im'''
    im = im.rotate(rotation,expand=1)
    return im
                           

def main(option):

    # Récupération du dossier de traitement
    rep = askdirectory()   
    liste = os.listdir(rep)
    liste_fic = [file for file in liste if '.png' in file or '.PNG' in liste
             or '.jpg' in file or '.jpeg' in file or '.JPG' in file or '.JPEG' in file]
    
    # Si pas d'image, fin de traitement
    if len(liste_fic) <1:
        print "Pas d'images à traiter dans le dossier"
        return

    # Bouclage sur la liste de fichier    
    for file in liste_fic:
        with Image.open(os.path.join(rep,file)) as im:

            # Récupération de la date (avant perte via utilisation module)
            if 'message' in option and option['message'] is 'date':
                try:
                    exif_data = im._getexif()
                    message = exif_data[306]
                    option['message'] = message
                except:
                    option['message'] = ''  
                    
            # Format de sortie (correction JPG/JPEG pour enregistrement)
            format_out = im.format           
            if format_out is 'JPG':
                format_out = 'JPEG'
            
            # Lancement des différents modules
            if 'pourcent' in option:
                im = resize(im,option['pourcent'])

            if 'rotation' in option:
                im = rotate(im,option['rotation'])

            if 'message' in option:
                watermark(im,option['message'])


            # Traitement du repertoire de sortie
            rep_out = os.path.join(rep,'Modified')

            if not os.path.isdir(rep_out):
                os.mkdir(rep_out)
                print 'Création du répertoire de sortie'

                
            # Sauvegarde
            im.save(os.path.join(rep_out,os.path.basename(file)), format_out)

            

    
###################### Scenario ###############################################
option = {'message':'date','pourcent':0.5}

main(option)  

